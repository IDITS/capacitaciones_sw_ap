﻿namespace AuraPortal_transferrerDocDictionaryLibrary
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSendQuery = new System.Windows.Forms.Button();
            this.btnSelectQuery = new System.Windows.Forms.Button();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtDomain = new System.Windows.Forms.TextBox();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblDomain = new System.Windows.Forms.Label();
            this.lblURL = new System.Windows.Forms.Label();
            this.fileDialogSelectQuery = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // btnSendQuery
            // 
            this.btnSendQuery.Enabled = false;
            this.btnSendQuery.Location = new System.Drawing.Point(366, 52);
            this.btnSendQuery.Name = "btnSendQuery";
            this.btnSendQuery.Size = new System.Drawing.Size(119, 23);
            this.btnSendQuery.TabIndex = 0;
            this.btnSendQuery.Text = "Send Query";
            this.btnSendQuery.UseVisualStyleBackColor = true;
            this.btnSendQuery.Click += new System.EventHandler(this.btnSendQuery_Click);
            // 
            // btnSelectQuery
            // 
            this.btnSelectQuery.Location = new System.Drawing.Point(366, 21);
            this.btnSelectQuery.Name = "btnSelectQuery";
            this.btnSelectQuery.Size = new System.Drawing.Size(119, 23);
            this.btnSelectQuery.TabIndex = 1;
            this.btnSelectQuery.Text = "Select Query";
            this.btnSelectQuery.UseVisualStyleBackColor = true;
            this.btnSelectQuery.Click += new System.EventHandler(this.btnSelectQuery_Click);
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(86, 23);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(100, 20);
            this.txtUser.TabIndex = 2;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(86, 49);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(100, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // txtDomain
            // 
            this.txtDomain.Location = new System.Drawing.Point(86, 75);
            this.txtDomain.Name = "txtDomain";
            this.txtDomain.Size = new System.Drawing.Size(100, 20);
            this.txtDomain.TabIndex = 4;
            // 
            // txtUrl
            // 
            this.txtUrl.Location = new System.Drawing.Point(86, 101);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(399, 20);
            this.txtUrl.TabIndex = 5;
            // 
            // lblUser
            // 
            this.lblUser.Location = new System.Drawing.Point(29, 31);
            this.lblUser.Name = "lblUser";
            this.lblUser.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblUser.Size = new System.Drawing.Size(55, 13);
            this.lblUser.TabIndex = 6;
            this.lblUser.Text = "User";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblPassword
            // 
            this.lblPassword.Location = new System.Drawing.Point(29, 56);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblPassword.Size = new System.Drawing.Size(55, 13);
            this.lblPassword.TabIndex = 7;
            this.lblPassword.Text = "Password";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDomain
            // 
            this.lblDomain.Location = new System.Drawing.Point(29, 82);
            this.lblDomain.Name = "lblDomain";
            this.lblDomain.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDomain.Size = new System.Drawing.Size(55, 13);
            this.lblDomain.TabIndex = 8;
            this.lblDomain.Text = "Domain";
            this.lblDomain.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblURL
            // 
            this.lblURL.Location = new System.Drawing.Point(29, 108);
            this.lblURL.Name = "lblURL";
            this.lblURL.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblURL.Size = new System.Drawing.Size(55, 13);
            this.lblURL.TabIndex = 9;
            this.lblURL.Text = "URL WS";
            this.lblURL.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // fileDialogSelectQuery
            // 
            this.fileDialogSelectQuery.DefaultExt = "xml";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 152);
            this.Controls.Add(this.lblURL);
            this.Controls.Add(this.lblDomain);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.txtUrl);
            this.Controls.Add(this.txtDomain);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.btnSelectQuery);
            this.Controls.Add(this.btnSendQuery);
            this.Name = "Form1";
            this.Text = "AuraPortal MoveDocDictionaryLibrary";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSendQuery;
        private System.Windows.Forms.Button btnSelectQuery;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtDomain;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblDomain;
        private System.Windows.Forms.Label lblURL;
        private System.Windows.Forms.OpenFileDialog fileDialogSelectQuery;
    }
}

