﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Diagnostics;
using AuraPortal_DocDictionaryLibrary.WS_AuraPortalDoc;

namespace AuraPortal_DocDictionaryLibrary
{
    public partial class Form1 : Form
    {
        #region Constants
        private const string _TagName_AP_DocumentLibrary = "AuraPortal_DocumentLibrary";
        private const string _TagName_DocumentName = "Name";
        private const string _TagName_DocumentContent = "Content";

        //Códigos de error
        public const int iError_Directory_UnauthorizedAccess = -1;
        public const int iError_Directory_PathTooLong = -2;
        public const int iError_Directory_CreateError = -3;
        public const int iError_File_UnauthorizedAccess = -4;
        public const int iError_File_FileNotFound = -5;
        public const int iError_File_DirectoryNotFound = -6;
        public const int iError_File_PathTooLong = -7;
        public const int iError_File_CreateError = -8;
        public const int iError_XML_NotValid = -9;
        public const int iError_Load_LibraryTerm = -10;
        public const int iError_Load_Process = -11;
        public const int iError_Load_OwnFamily = -12;
        public const int iError_Load_OwnFamilyElement = -13;
        public const int iError_Load_PersonalRole = -14;
        public const int iError_Load_IntegratedDocument = -15;
        public const int iError_DocSigned_CanNotBeChanged = -16;
        #endregion

        public Form1()
        {
            InitializeComponent();            
        }

        /// <summary>
        /// Integra un Documento en AuraPortal, se hace con una única llamada al SW.
        /// </summary>
        private void SendQuery()
        {
            string xmlDocument = File.ReadAllText(fileDialogSelectQuery.FileName);

            if (!ValidateDocumentXML(xmlDocument))
                return;
            
            using (AuraPortalDoc oAuraPortalDoc = Get_WebService())
            {
                string returnValue = oAuraPortalDoc.updateDocDictionaryLibraryLabels(xmlDocument).Trim();
                int returnCode = 0;

                if (returnValue.Length > 0)
                {
                    if (Int32.TryParse(returnValue, out returnCode) && returnCode < 0)
                    {
                        ShowErrorMessage(returnCode);
                    }
                    else
                    {
                        ShowInformationMessage("Successfully updated Document Labels.... Document Token [" + returnValue + "]");                        
                    }
                }
                else
                    ShowInformationMessage("Empty document downloaded...");
            }
        }

        /// <summary>
        /// Valida el Documento XML mediante el esquema xsd.
        /// </summary>
        /// <param name="xmlData">Documento XML</param>        
        private bool ValidateDocumentXML(string xmlData)
        {
            bool result = false;

            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.LoadXml(xmlData);
            oXmlDocument.Schemas.Add(null, "updateDocDictionaryLibraryLabels.xsd");

            try
            {
                oXmlDocument.Validate(new ValidationEventHandler(ValidationEvent));
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Error validating XML Document.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return result;
        }
        
        private void ValidationEvent(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    throw new Exception(e.Message);
                case XmlSeverityType.Warning:
                    throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Prepara y muestra los mensajes de error según el código de error recibido.
        /// </summary>
        /// <param name="returnCode">código de error</param>
        private void ShowErrorMessage(int returnCode)
        {
            string errorMessage = string.Empty;
            switch (returnCode)
            {
                case iError_Directory_UnauthorizedAccess:
                    errorMessage = "Message for error -1";
                    break;
                case iError_Directory_PathTooLong:
                    errorMessage = "Message for error -2";
                    break;
                case iError_Directory_CreateError:
                    errorMessage = "Message for error -3";
                    break;
                case iError_File_UnauthorizedAccess:
                    errorMessage = "Message for error -4";
                    break;
                case iError_File_FileNotFound:
                    errorMessage = "Message for error -5";
                    break;
                case iError_File_DirectoryNotFound:
                    errorMessage = "Message for error -6";
                    break;
                case iError_File_PathTooLong:
                    errorMessage = "Message for error -7";
                    break;
                case iError_File_CreateError:
                    errorMessage = "Message for error -8";
                    break;
                case iError_XML_NotValid:
                    errorMessage = "Message for error -9";
                    break;
                case iError_Load_LibraryTerm:
                    errorMessage = "Message for error -10";
                    break;
                case iError_Load_Process:
                    errorMessage = "Message for error -11";
                    break;
                case iError_Load_OwnFamily:
                    errorMessage = "Message for error -12";
                    break;
                case iError_Load_OwnFamilyElement:
                    errorMessage = "Message for error -13";
                    break;
                case iError_Load_PersonalRole:
                    errorMessage = "Message for error -14";
                    break;
                case iError_Load_IntegratedDocument:
                    errorMessage = "Message for error -15";
                    break;
                case iError_DocSigned_CanNotBeChanged:
                    errorMessage = "Message for error -16";
                    break;
            }

            MessageBox.Show("Error ... Error Code [" + returnCode + "]", "GetDocDictionaryLibraryList", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Prepara y muestra mensajes de información.
        /// </summary>
        /// <param name="returnCode">código de error</param>
        private void ShowInformationMessage(string message)
        {
            MessageBox.Show(message, "GetDocDictionaryLibraryList", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Obtiene el objeto que se encarga de la invocación del servicio web.
        /// </summary>
        /// <returns>Servicio Web</returns>
        private AuraPortalDoc Get_WebService()
        {
            using (AuraPortalDoc oAuraPortalDoc = new AuraPortalDoc())
            {
                NetworkCredential credential = new NetworkCredential(txtUser.Text, txtPassword.Text, txtDomain.Text);

                oAuraPortalDoc.Url = txtUrl.Text;
                oAuraPortalDoc.Credentials = credential;

                return oAuraPortalDoc;
            }
        }

        #region Button Events
        private void btnSendQuery_Click(object sender, EventArgs e)
        {
            try
            {
                SendQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSelectQuery_Click(object sender, EventArgs e)
        {
            fileDialogSelectQuery.InitialDirectory = Environment.CurrentDirectory;
            fileDialogSelectQuery.Filter = "XML files | *.xml";

            DialogResult dialogResult = fileDialogSelectQuery.ShowDialog();

            switch (dialogResult)
            {
                case DialogResult.OK:
                case DialogResult.Yes:
                    btnSendQuery.Enabled = true;
                    break;
                default:
                    btnSendQuery.Enabled = false;
                    break;
            }
        }
        #endregion
    }
}
