﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AuraPortal_UploadDocToDictionaryLibrary.AuraPortalDoc_WS;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Diagnostics;

namespace AuraPortal_UploadDocToDictionaryLibrary
{
    public partial class Form1 : Form
    {
        #region Constants
        private const string _XmlDocument_Test = @"Document_Test.xml";
        private const string _XmlDocument_Test_Template = @"Document_Template.xml";
        private const int _MaxFileSize_UniqueUpload = 20971520;
        private const int _MaxPartFileSize_DividedUpload = 4194304;

        private const string _Part_Start = "sta";
        private const string _Part_Middle = "mid";
        private const string _Part_End = "end";

        private const string _TagName_AP_DocumentLibrary = "AuraPortal_DocumentLibrary";
        private const string _TagName_DocumentName = "Library_Name";
        private const string _TagName_DocumentContent = "Content";

        //Códigos de error
        public const int iError_Directory_UnauthorizedAccess = -1;
        public const int iError_Directory_PathTooLong = -2;
        public const int iError_Directory_CreateError = -3;
        public const int iError_File_UnauthorizedAccess = -4;
        public const int iError_File_FileNotFound = -5;
        public const int iError_File_DirectoryNotFound = -6;
        public const int iError_File_PathTooLong = -7;
        public const int iError_File_CreateError = -8;
        public const int iError_XML_NotValid = -9;
        public const int iError_Load_LibraryTerm = -10;
        public const int iError_Load_Process = -11;
        public const int iError_Load_OwnFamily = -12;
        public const int iError_Load_OwnFamilyElement = -13;
        public const int iError_Load_PersonalRole = -14;
        #endregion

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Integra el Documento en AuraPortal, según el tamaño del mismo :
        /// - (Menor de 20MB)lo hace en una sola llamada al SW.
        /// - (Mayor de 20MB) lo divide en partes de 4MB y realiza tantas llamadas al SW como partes hayan.
        /// </summary>
        private void UploadDocument()
        {
            FileInfo fileInfo = new FileInfo(_XmlDocument_Test);

            if (fileInfo.Exists)
            {
                if (!ValidateDocumentXML(File.ReadAllText(_XmlDocument_Test)))
                    return;

                if (fileInfo.Length > _MaxFileSize_UniqueUpload)
                {
                    UploadDocument_Divided();
                }
                else
                {
                    UploadDocument_Unique();
                }
            }
            else
                MessageBox.Show("XML File " + _XmlDocument_Test + " not found.", "Error Open XML Document.", MessageBoxButtons.OK, MessageBoxIcon.Error);


            fileInfo = null;
        }
        

        /// <summary>
        /// Integra un Documento en AuraPortal, se hace con una única llamada al SW.
        /// </summary>
        private void UploadDocument_Unique()
        {
            using (AuraPortalDoc oAuraPortalDoc = Get_WebService())
            {
                string xmlDocument = File.ReadAllText(_XmlDocument_Test);
                string returnValue = oAuraPortalDoc.uploadDocumentToDictionaryLibrary_Unique(xmlDocument);
                int returnCode = 0;

                if (returnValue.Length > 0)
                {
                    if (Int32.TryParse(returnValue, out returnCode) && returnCode < 0)
                    {
                        ShowErrorMessage(returnCode);
                    }
                    else
                    {
                        ShowInformationMessage(returnValue);
                    }
                }
            }
        }

        /// <summary>
        /// Integra un Documento en AuraPortal, el documento se divide en partes de 4MB,
        /// se realizarán tantas llamadas al SW como partes hayan.
        /// </summary>
        private void UploadDocument_Divided()
        {
            using (AuraPortalDoc oAuraPortalDoc = Get_WebService())
            {
                byte[] documentContent = File.ReadAllBytes(_XmlDocument_Test);
                byte[] bufferContent = null;
                List<byte> fich = new List<byte>();
                int returnCode = 0;
                bool upload_Part_End = false;

                string idTempFile = string.Empty;

                using (MemoryStream strRead = new MemoryStream(documentContent))
                {
                    bool isPartStart = true;

                    while (strRead.Position < strRead.Length)
                    {
                        bufferContent = null;
                        fich.Clear();
                        returnCode = 0;

                        if (strRead.Length - strRead.Position >= _MaxPartFileSize_DividedUpload)
                        {
                            bufferContent = new byte[_MaxPartFileSize_DividedUpload];
                            strRead.Read(bufferContent, 0, _MaxPartFileSize_DividedUpload);

                            if (isPartStart)
                            {
                                //Primera llamada al Servicio Web, se inserta la cadena "sta" para identificarla.
                                fich.AddRange(Encoding.Default.GetBytes(_Part_Start));
                                isPartStart = false;
                            }
                            else
                            {
                                //Llamada intermedia al Servicio Web, se inserta la cadena "mid" para identificarla.
                                fich.AddRange(Encoding.Default.GetBytes(_Part_Middle));
                            }
                        }
                        else
                        {
                            bufferContent = new byte[strRead.Length - strRead.Position];
                            strRead.Read(bufferContent, 0, (int)(strRead.Length - strRead.Position));
                            //Ultima llamada al Servicio Web, se inserta la cadena "end" para identificarla.
                            fich.AddRange(Encoding.Default.GetBytes(_Part_End));
                            upload_Part_End = true;
                        }

                        //Insertamos una parte del contenido.
                        fich.AddRange(bufferContent);

                        //Información SW uploadDocumentToDictionaryLibrary_Divided:
                        //Integra Documentos por partes, es decir, para la integración completa de un Documento serán necesarias varias llamadas al Servicio Web.
                        //Parámetro (byte[] data): parte del fichero a subir.
                        //Parámetro (string idTmpFile): código identificador del fichero que se está integrado, la primera llamada al Servicio Web se ha de hacer con una cadena de texto vacio.
                        //Devuelve (string): devuelve el código identificador del fichero que se está integrando, se utiliza en la segunda y sucesivas llamadas al servicio web. La devoluación de la última llamada es el Id del Documento Integrado.
                        idTempFile = oAuraPortalDoc.uploadDocumentToDictionaryLibrary_Divided(fich.ToArray(), idTempFile);

                        if (idTempFile.Length > 0)
                        {
                            if (Int32.TryParse(idTempFile, out returnCode) && returnCode < 0)
                            {
                                ShowErrorMessage(returnCode);
                            }
                            else if (upload_Part_End)
                            {
                                ShowInformationMessage(idTempFile);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Valida el Documento XML mediante el esquema xsd.
        /// </summary>
        /// <param name="xmlData">Documento XML</param>        
        private bool ValidateDocumentXML(string xmlData)
        {
            bool result = false;

            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.LoadXml(xmlData);
            oXmlDocument.Schemas.Add(null, "uploadDocumentToDictionaryLibrary.xsd");

            try
            {
                oXmlDocument.Validate(new ValidationEventHandler(ValidationEvent));
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message, "Error validating XML Document.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return result;
        }

        private void ValidationEvent(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    throw new Exception(e.Message);
                case XmlSeverityType.Warning:
                    throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Prepara y muestra los mensajes de error según el código de error recibido.
        /// </summary>
        /// <param name="returnCode">código de error</param>
        private void ShowErrorMessage(int returnCode)
        {   
            string errorMessage = string.Empty;
            switch (returnCode)
            {
                case iError_Directory_UnauthorizedAccess:
                    errorMessage = "Message for error -1";
                    break;
                case iError_Directory_PathTooLong:
                    errorMessage = "Message for error -2";
                    break;
                case iError_Directory_CreateError:
                    errorMessage = "Message for error -3";
                    break;
                case iError_File_UnauthorizedAccess:
                    errorMessage = "Message for error -4";
                    break;
                case iError_File_FileNotFound:
                    errorMessage = "Message for error -5";
                    break;
                case iError_File_DirectoryNotFound:
                    errorMessage = "Message for error -6";
                    break;
                case iError_File_PathTooLong:
                    errorMessage = "Message for error -7";
                    break;
                case iError_File_CreateError:
                    errorMessage = "Message for error -8";
                    break;
                case iError_XML_NotValid:
                    errorMessage = "Message for error -9";
                    break;
                case iError_Load_LibraryTerm:
                    errorMessage = "Message for error -10";
                    break;
                case iError_Load_Process:
                    errorMessage = "Message for error -11";
                    break;
                case iError_Load_OwnFamily:
                    errorMessage = "Message for error -12";
                    break;
                case iError_Load_OwnFamilyElement:
                    errorMessage = "Message for error -13";
                    break;
                case iError_Load_PersonalRole:
                    errorMessage = "Message for error -14";
                    break;
            }

            MessageBox.Show("Error ... Error Code [" + returnCode + "]", "UploadDocumentToDictionaryLibrary", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Prepara y muestra mensajes de información.
        /// </summary>
        /// <param name="returnCode">código de error</param>
        private void ShowInformationMessage(string documentToken)
        {
            MessageBox.Show("Document uploaded ... Document Token [" + documentToken + "]", "UploadDocumentToDictionaryLibrary", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Muestra un dialogo para la selección de un fichero.
        /// </summary>
        /// <param name="fileInfo">Parametro que devuelve la información del fichero seleccionado</param>
        /// <returns>Contenido binario del fichero seleccionado</returns>
        private byte[] SelectDocument(out FileInfo fileInfo)
        {
            byte[] contentFile = null;
            fileInfo = null;

            using (OpenFileDialog oFileDialog = new OpenFileDialog())
            {
                if (oFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileInfo = new FileInfo(oFileDialog.FileName);
                    contentFile = File.ReadAllBytes(oFileDialog.FileName);
                }
            }

            return contentFile;
        }

        /// <summary>
        /// Crea el Documento XML a partir de:
        /// - Un documento xml utilizado como plantilla.
        /// - La información del fichero pasada como parámetro.
        /// - El contenido del fichero pasado como parámetro.
        /// </summary>
        /// <param name="fileInfo">Información del fichero</param>
        /// <param name="content">Contenido del fichero</param>
        private void CreateDocument(FileInfo fileInfo, byte[] content)
        {
            if (content == null)
                return;

            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.LoadXml(File.ReadAllText(_XmlDocument_Test_Template, Encoding.Default));

            XmlNodeList nodeList_Root_DocLibray = oXmlDocument.GetElementsByTagName(_TagName_AP_DocumentLibrary);
            XmlNodeList nodeList_Content = oXmlDocument.GetElementsByTagName(_TagName_DocumentContent);
            XmlNodeList nodeList_DocName = oXmlDocument.GetElementsByTagName(_TagName_DocumentName);

            if (nodeList_DocName.Count > 0)
            {
                nodeList_DocName[0].InnerText = fileInfo.Name;
            }

            if (nodeList_Content.Count > 0)
            {
                nodeList_Content[0].InnerText = Convert.ToBase64String(content);
            }
            else
            {
                XmlNode oXmlNode = oXmlDocument.CreateNode(XmlNodeType.Element, _TagName_DocumentContent, oXmlDocument.NamespaceURI);
                oXmlNode.InnerText = Convert.ToBase64String(content);

                if (nodeList_Root_DocLibray.Count > 0)
                    nodeList_Root_DocLibray[0].AppendChild(oXmlNode);
            }

            File.Delete(_XmlDocument_Test);
            oXmlDocument.Save(_XmlDocument_Test);

            MessageBox.Show("XML Document created.");
        }

        /// <summary>
        /// Obtiene el objeto que se encarga de la invocación del servicio web.
        /// </summary>
        /// <returns>Servicio Web</returns>
        private AuraPortalDoc Get_WebService()
        {
            using (AuraPortalDoc oAuraPortalDoc = new AuraPortalDoc())
            {
                NetworkCredential credential = new NetworkCredential(txtUser.Text, txtPassword.Text, txtDomain.Text);

                oAuraPortalDoc.Url = txtUrl.Text;
                oAuraPortalDoc.Credentials = credential;

                return oAuraPortalDoc;
            }
        }

        #region Button Events
        private void btoUpload_Click(object sender, EventArgs e)
        {
            UploadDocument();
        }

        private void btoDocCreate_Click(object sender, EventArgs e)
        {
            FileInfo fileInfo = null;
            byte[] contentFile = SelectDocument(out fileInfo);

            if (contentFile != null)
            {
                CreateDocument(fileInfo, contentFile);
            }
        }
        #endregion
    }
}
